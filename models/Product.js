
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let productSchema = new Schema({

    productName:{
      type: String,
      required: true
     },
    description: {
      type: String,
      required: true
     },
    status: {
    type: String,
    enum : ['To Buy','Bought'],
    default: 'To Buy'
    },
    price: {
      type: Number,
      required: true
    },
    currency: {
        type: String,
        enum : ['TND','EURO','USD'],
        default: 'TND'
        },
    image: {
      type: String,
    },
    wish: { type: Schema.Types.ObjectId, ref: 'Wish'}
},{
   timestamps: true,
   collection: 'products'
})
module.exports = mongoose.model('Product', productSchema);