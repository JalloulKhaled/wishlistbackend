const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let wishSchema = new Schema({
   wishName:{
      type: String, 
      unique: true ,
      required: true
   },
   user: { type: Schema.Types.ObjectId, ref: 'User' },
   products: [{ type: Schema.Types.ObjectId, ref: 'Product' }]
},{
   timestamps: true,
   collection: 'wishes'
})
module.exports = mongoose.model('Wish', wishSchema);