const express = require('express');
const mongoose = require('mongoose');
const Product = require('../models/Product');
const route = express.Router();

exports.addproduct = async (req, res) => {
  let { productName , description, status ,currency, price ,image, wish} = req.body;
  let product = {};
  product.productName = productName;
  product.description = description;
  product.status = status;
  product.price = price;
  product.currency = currency;
  product.image = image;
  product.wish = wish;

  let productModel = new Product(product);
  await productModel.save();
  res.json(productModel);
}
exports.getAllproducts = async(req, res) => {
    let { wish } = req.body;
    Product.find({ wish: wish}, function(err, result) {
      if (err) {
        console.log(err);
        res.status(500).json({ errors: "wrong list" });
      } else {
        res.json(result);
      }
    });
  }
  