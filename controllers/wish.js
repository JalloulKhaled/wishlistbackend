const express = require('express');
const mongoose = require('mongoose');
const Wish = require('../models/Wish');
const route = express.Router();

exports.addwish = async (req, res) => {
  let { wishName , user } = req.body;
  let wish = {};
  wish.wishName = wishName;
  wish.user = user;
  let wishModel = new Wish(wish);
  await wishModel.save();
  res.json("success");
}

exports.getAllWishes = async(req, res) => {
  let { user } = req.body;
  Wish.find({ user: user}, function(err, result) {
    if (err) {
      console.log(err);
      res.status(500).json({ errors: "wrong user" });
    } else {
      res.json(result);
    }
  });
}